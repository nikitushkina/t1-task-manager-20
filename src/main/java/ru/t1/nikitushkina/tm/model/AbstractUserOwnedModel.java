package ru.t1.nikitushkina.tm.model;

public abstract class AbstractUserOwnedModel extends AbstractModel {

    private String UserId;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        this.UserId = userId;
    }

}
