package ru.t1.nikitushkina.tm.api.repository;

import ru.t1.nikitushkina.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

    void clear();

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

}
