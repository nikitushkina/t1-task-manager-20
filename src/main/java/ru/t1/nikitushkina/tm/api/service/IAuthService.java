package ru.t1.nikitushkina.tm.api.service;

import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.model.User;

public interface IAuthService {

    void checkRoles(Role[] roles);

    User getUser();

    String getUserId();

    boolean isAuth();

    void login(String login, String password);

    void logout();

    User registry(String login, String password, String email);

}
