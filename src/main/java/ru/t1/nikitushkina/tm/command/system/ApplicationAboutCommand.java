package ru.t1.nikitushkina.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";

    public static final String DESCRIPTION = "Display developer info.";

    public static final String ARGUMENT = "-a";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Olga Nikitushkina");
        System.out.println("email: onikitushkina@t1-consulting.ru ");
    }

}
