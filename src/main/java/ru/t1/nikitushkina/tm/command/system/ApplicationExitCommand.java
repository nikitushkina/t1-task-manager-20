package ru.t1.nikitushkina.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    public static final String NAME = "exit";

    public static final String DESCRIPTION = "Close application.";

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

}
