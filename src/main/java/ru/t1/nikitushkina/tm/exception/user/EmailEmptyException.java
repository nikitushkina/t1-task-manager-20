package ru.t1.nikitushkina.tm.exception.user;

public final class EmailEmptyException extends AbstractUserException {

    public EmailEmptyException() {
        super("Error! Email is empty.");
    }

}
